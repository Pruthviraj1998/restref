package restAssuredRefrence;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class GetRefrence {

	public static void main(String[] args) {
		// Step 1 : Declare the variable for Base URI and request body
		String BaseURI = "https://reqres.in/";

		// Step 2 : Declare BaseURI

		RestAssured.baseURI = BaseURI;

		// Step 3 : Configure RequestBody and trigger the API

//		String ResponseBody = given().when().get("api/users/2").then().extract().response().asString();
//		
//		System.out.println(ResponseBody);

		String ResponseBody = given().when().get("api/users/2").then().extract().response().asString();
//		System.out.println(ResponseBody);

		// Create an object of json path to parse the Response body.

		JsonPath jsp_res = new JsonPath(ResponseBody);

		int res_id = jsp_res.getInt("data.id");
		System.out.println(res_id);

		String res_email = jsp_res.getString("data.email");
		System.out.println(res_email);

		String res_first_name = jsp_res.getString("data.first_name");
		System.out.println(res_first_name);

		String res_last_name = jsp_res.getString("data.last_name");
		System.out.println(res_last_name);

		String res_avatar = jsp_res.getString("data.avatar");
		System.out.println(res_avatar);

		// Validate the response body parameters (testNG)

		Assert.assertEquals(res_id, 2);
		Assert.assertEquals(res_email, "janet.weaver@reqres.in");
		Assert.assertEquals(res_first_name, "Janet");
		Assert.assertEquals(res_last_name, "Weaver");
		Assert.assertEquals(res_avatar, "https://reqres.in/img/faces/2-image.jpg");

	}

}