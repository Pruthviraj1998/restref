package  restAssuredRefrence;

import static io.restassured.RestAssured.given;


import java.time.LocalDateTime;


import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class PatchRefrence {

	public static void main(String[] args) {
		// Step 1 : Declare the variable for BaseURI and requestBody

		String BaseURI = "https://reqres.in/";
		String RequestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		String Resource = "api/users/2";

		// Step 2 : Declare BaseURI

		RestAssured.baseURI = BaseURI;

		// Step 3 : Configure RequestBody and trigger the API

//		String ResponseBody = given().header("Content-Type", "application/json").body(RequestBody).log().all().when().patch(Resource).then()
//				.log().all().extract().response().asString();

		String ResponseBody = given().header("Content-Type", "application/json").body(RequestBody).when()
				.patch(Resource).then().extract().response().asString();

		// Create an object of JsonPath to parse the Response body.

		JsonPath jsp_res = new JsonPath(ResponseBody);

		String res_name = jsp_res.getString("name");
		System.out.println(res_name);

		String res_job = jsp_res.getString("job");
		System.out.println(res_job);

		String res_updatedAt = jsp_res.getString("updatedAt");


		String updatedDate = res_updatedAt.substring(0, 10);
		System.out.println(updatedDate);

		LocalDateTime Date = LocalDateTime.now();

		String newDate = Date.toString();

		String CurrentDate = newDate.substring(0, 10);
		System.out.println(CurrentDate);
		
		// Create an object of JsonPath to parse the Request body.

		JsonPath jsp_req = new JsonPath(RequestBody);

		String req_name = jsp_req.getString("name");
		System.out.println(req_name);

		String req_job = jsp_req.getString("job");
		System.out.println(req_job);

		// Validate the response body parameters (testNG)

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(updatedDate, CurrentDate);

	}

}