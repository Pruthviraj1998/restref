package restAssuredRefrence;

import io.restassured.RestAssured;

import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;


import org.testng.Assert;

public class PostRefrence {

	public static void main(String[] args) {
		
		
		// step 1 : Declare the variable for base URI and Request body.

		
		
		
				String BaseURI = "https://reqres.in/";
				String RequestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
//				
//				System.out.println(RequestBody);

				// Step 2 : Declare BaseURI
				RestAssured.baseURI = BaseURI;

//				given().header("Content-Type", "application/json").body(RequestBody).log().all().when().post("api/users")
//				.then().log().all().extract().response().asString();

				// Step 3 : Configure RequestBody and trigger the API

				String ResponseBody = given().header("Content-Type", "application/json").body(RequestBody).when()
						.post("api/users").then().extract().response().asString();		 
				System.out.println(ResponseBody);
				
				
				
// create an object of j 
				
			//	json path to parse request body
				
				
				JsonPath jsp_req = new JsonPath(RequestBody);

				String req_name = jsp_req.getString("name");
				System.out.println("Request body parameter : " + req_name);
				String req_job = jsp_req.getString("job");
				System.out.println("Request body parameter : " + req_job);


				// Create an object of json path to parse the Response body.

				JsonPath jsp_res = new JsonPath(ResponseBody);

				String res_name = jsp_res.getString("name");
				System.out.println("Response body parameter : " + res_name);

				String res_job = jsp_res.getString("job");
				System.out.println("Response body parameter : " + res_job);

			    String res_id = jsp_res.getString("id");
				System.out.println("Generated ID : " + res_id);

				String res_createdAt = jsp_res.getString("createdAt");

				String generatedDate = res_createdAt.substring(0, 10);
				System.out.println(generatedDate);

				LocalDateTime CurrentDate = LocalDateTime.now();

				String newDate = CurrentDate.toString();

				String updatedDate = newDate.substring(0, 10);
				System.out.println(updatedDate);


				// Validation

				Assert.assertEquals(res_name, req_name);
				Assert.assertEquals(res_job, req_job);
				Assert.assertEquals(generatedDate, updatedDate);
				Assert.assertNotNull(res_id);
		
	}

}
